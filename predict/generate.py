import random
import sys

from game.do.tokens import Tokens
from game.game import Game


class Generator:
    def __init__(self, options=None):
        if not options:
            options = {}

        defaults = {
            'max_turns': 60,
            'logging': True,
            'number_of_tokens_per_type': None,
            'points_limit': 15,
            'reserved_cards_limit': 3,
            'seed': None
        }

        self.options = {**defaults, **options}

    def generate(self, game=None):
        if game is None:
            game = Game()

        actions = []
        self.configure_game(game)

        self.log('Game started!')
        while game.turns < self.option('max_turns'):
            player = game.get_current_player()

            can_build, card = self.can_build(game, player)
            can_take_tokens, tokens = self.can_take_tokens(game, player)
            if not can_build and not can_take_tokens:
                actions.append({
                    "kind": "skip_turn",
                    "player": player.id,
                    "card": card
                })
                self.log("{} skipped his turn", player)
                game.next_player()
                continue

            if can_build and can_take_tokens:
                ratio = 1 / 2
                action = random.uniform(0, 1)

                if action <= ratio:
                    self.log('{} took {}', player, tokens)
                    actions.append({
                        "kind": "take_tokens",
                        "player": player.id,
                        "tokens": tokens
                    })
                    game.take_tokens(tokens)
                else:
                    self.log('{} built {}', player, card)
                    actions.append({
                        "kind": "built_card",
                        "player": player.id,
                        "card": card
                    })
                    game.build_card(card)

                game.next_player()
                continue

            if can_build and not can_take_tokens:
                actions.append({
                    "kind": "built_card",
                    "player": player.id,
                    "card": card
                })
                self.log('{} built {}', player, card)
                game.build_card(card)

            if can_take_tokens and not can_build:
                actions.append({
                    "kind": "take_tokens",
                    "player": player.id,
                    "tokens": tokens
                })
                self.log('{} took {}', player, tokens)
                game.take_tokens(tokens)

            game.next_player()
        return {
            "turns": game.turns,
            "seed": self.option('seed'),
            "actions": actions,
            "game": game,
            "options": self.options
        }

    def can_build(self, game, player):
        cards = []
        for k, age in enumerate(game.discovered_cards):
            for card in age:
                if len(player.cards) > 6 and k == 0:
                    continue

                if card.cost in player.get_all_tokens():
                    cards.append(card)

        for reserved_card in player.reserved_cards:
            if reserved_card.cost in player.get_all_tokens():
                cards.append(reserved_card)

        if not cards:
            return [False, None]

        return [True, random.choice(cards)]

    def can_take_tokens(self, game, player):
        if game.tokens.total() == 0:
            return [False, None]

        if player.tokens.total() >= 10:
            return [False, None]

        if game.tokens.total() > 3 and player.tokens.total() > 7:
            return [False, None]

        tokens = ''

        available = game.tokens.get_available_types(['m'])
        random.shuffle(available)

        for token in available:
            tokens += token

            if len(tokens) == 3:
                break

        return [True, Tokens.from_str(tokens)]

    def can_reserve_card(self, game, player):
        if len(player.reserved_cards) == 3:
            return [False, None]

        cards = [game.reserve_card_from_the_stack(i) for i in 3]

        for age in game.discovered_cards:
            for card in age:
                cards.append(card)

        return [True, random.choice(cards)]

    def log(self, message, *context):
        if self.option('logging'):
            if isinstance(message, str):
                print(message.format(*context))
            else:
                print(message)

    def option(self, name):
        return self.options[name]

    def configure_game(self, game):
        tokens_per_type = self.option('number_of_tokens_per_type')
        if tokens_per_type:
            game.number_of_tokens_per_type = tokens_per_type

        if not self.option('seed'):
            seed = random.randrange(sys.maxsize)
            random.Random(seed)
            self.options['seed'] = seed

        random.seed(self.option('seed'))

        game.points_limit = self.option('points_limit')
        game.reserved_cards_limit = self.option('reserved_cards_limit')
