import statistics

from game.do.card import Card
from game.do.noble import Noble
from game.do.player import Player
from game.do.tokens import Tokens
from game.game import Game
from predict.generate import Generator


class IA:

    def best(self, game, n=1000):
        generations = []

        for _ in range(n):
            play = self.clone_game(game)
            generator = Generator({
                "max_turns": 60,
                "logging": False
            })
            generation = generator.generate(play)

            generations.append(generation)

        avgs = []
        for gen in generations:
            avgs.append(
                (
                        gen['game'].players[0].points +
                        gen['game'].players[1].points +
                        gen['game'].players[2].points
                ) / 3
            )

        print("max: {}".format(max(avgs)))
        print("min: {}".format(min(avgs)))
        print("median: {}".format(statistics.median(avgs)))
        print("avg: {}".format(sum(avgs) / len(generations)))
        print("Using {} generations".format(len(generations)))

    def clone_game(self, game):
        new_game = Game()
        new_game.discovered_cards = [[], [], []]

        for card in game.cards:
            new_game.cards.append(
                Card(card.age, card.cost, card.production, card.points)
            )

        for k, age in enumerate(game.discovered_cards):
            for card in age:
                new_game.discovered_cards[k].append(
                    Card(k, card.cost, card.production, card.points)
                )

        for noble in game.discovered_nobles:
            new_game.discovered_nobles.append(
                Noble(noble.requirements, noble.points)
            )

        for noble in game.nobles:
            new_game.nobles.append(
                Noble(noble.requirements, noble.points)
            )

        for player in game.players:
            new_game.players.append(
                Player(player.name)
            )

        new_game.number_of_tokens_per_type = len(game.players) + 2
        return new_game
