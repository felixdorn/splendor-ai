from copy import deepcopy

from game.game import Game
from predict.ia import IA

game = Game()
game.prepare()
IA().best(game)