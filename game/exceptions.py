class SplendorException(Exception):
    pass


class InvalidAmountException(SplendorException):
    pass


class ReservedCardsInventoryFullException(SplendorException):
    pass


class CardTooExpensiveException(SplendorException):
    pass


class MagicTokenRequestException(SplendorException):
    pass


class EndOfStackException(SplendorException):
    pass


class NotFamousEnoughException(SplendorException):
    pass
