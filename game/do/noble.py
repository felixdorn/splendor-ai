class Noble:
    def __init__(self, requirements, points=3):
        self.requirements = requirements
        self.points = points

    def __repr__(self):
        return f"Noble({self.requirements}) -> {self.points}"
