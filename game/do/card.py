class Card:
    def __init__(self, age, cost, production, points=0):
        self.age = age
        self.cost = cost
        self.production = production
        self.points = points

    def __repr__(self):
        return f"Card({self.cost}) -> {self.points}, {self.production}"
