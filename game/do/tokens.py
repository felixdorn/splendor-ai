import random

from game.exceptions import InvalidAmountException


class Tokens(dict):
    def __init__(self, e=0, s=0, r=0, d=0, o=0, m=0):
        super().__init__({
            "e": e,
            "s": s,
            "r": r,
            "d": d,
            "o": o,
            "m": m,
        })


    def has(self, token, min=1):
        return self[token] >= min

    def remove(self, token, qt=1, with_magic=False):
        if self[token] >= qt:
            self[token] -= qt
            return

    def add(self, token, qt=1):
        if qt == 0:
            return

        self[token] += qt

    @classmethod
    def from_str(cls, tokens):
        return cls(
            e=tokens.count('e'),
            s=tokens.count('s'),
            r=tokens.count('r'),
            d=tokens.count('d'),
            o=tokens.count('o'),
            m=tokens.count('m'),
        )

    def total(self):
        return sum(self.values())

    def get_available_types(self, excepted=None):
        if excepted is None:
            excepted = []

        types = []

        for token, qt in self.items():
            if qt > 0 and token not in excepted:
                types.append(token)

        return types

    def remove_all(self, tokens, with_magic=False):
        for token, qt in tokens.items():
            self.remove(token, qt, with_magic)
        return self

    def add_all(self, tokens):
        for token, qt in tokens.items():
            self.add(token, qt)

        return self

    def random(self, excepted=None):
        if excepted is None:
            excepted = []

        return random.choice(self.get_available_types(excepted))

    def clone(self) -> "Tokens":
        return Tokens(
            e=self['e'],
            s=self['s'],
            r=self['r'],
            d=self['d'],
            o=self['o'],
            m=self['m'],
        )

    def __add__(self, other):
        if other.total() == 0:
            return self

        return self.clone().add_all(other)

    def __sub__(self, other):
        if other.total() == 0:
            return self

        return self.clone().remove_all(other)

    def __repr__(self, tokens=None):
        if self.total() == 0:
            return 'no tokens'

        tokens = ''

        for token, qt in self.items():
            if qt > 0:
                tokens += f"{qt}{token}"

        return tokens

    def __contains__(self, item):
        if item is None:
            return False

        for token, qt in item.items():
            if not self.has(token, qt):
                return False
        return True
