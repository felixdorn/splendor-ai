import uuid

from game.do.tokens import Tokens


class Player:
    def __init__(self, name):
        self.id = name
        self.name = name
        self.tokens = Tokens()
        self.cards = []
        self.reserved_cards = []
        self.nobles = []
        self.points = 0
        self.turns_played = 0

    def get_all_tokens(self):
        return self.tokens + self.get_cards_production()

    def get_cards_production(self):
        tokens = Tokens()
        for card in self.cards:
            tokens.add_all(card.production)

        return tokens

    def __repr__(self):
        return f"{self.name} -> {self.points} (t={self.tokens}, c={len(self.cards)},r={len(self.reserved_cards)},n={len(self.nobles)})"
