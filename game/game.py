import json
import os
import random

from game.do.card import Card
from game.do.noble import Noble
from game.do.player import Player
from game.do.tokens import Tokens
from game.exceptions import InvalidAmountException, ReservedCardsInventoryFullException, CardTooExpensiveException, \
    MagicTokenRequestException, EndOfStackException, NotFamousEnoughException


def load_cards():
    with open(os.path.dirname(__file__) + '/data/cards.json', 'r') as file:
        cards = json.loads(file.read())

    for k, card in enumerate(cards):
        cards[k] = Card(
            int(card['age']),
            Tokens.from_str(card['cost']),
            Tokens.from_str(card['production']),
            int(card['points']))

    return cards


def load_nobles():
    with open(os.path.dirname(__file__) + '/data/nobles.json', 'r') as file:
        nobles = json.loads(file.read())

    for k, noble in enumerate(nobles):
        nobles[k] = Noble(Tokens.from_str(noble))

    return nobles


class Game:
    def __init__(self):
        self.players = []
        self.cards = []
        self.nobles = []
        self.number_of_tokens_per_type = 5
        self.tokens = Tokens(
            e=self.number_of_tokens_per_type,
            s=self.number_of_tokens_per_type,
            r=self.number_of_tokens_per_type,
            d=self.number_of_tokens_per_type,
            o=self.number_of_tokens_per_type,
            m=5,
        )
        self.points_limit = 15
        self.turns = 0
        self.reserved_cards_limit = 3
        self.playing = 0
        self.discovered_cards = []
        self.discovered_nobles = []

    def prepare(self, players=None, cards=load_cards(), nobles=load_nobles()):
        if players is None:
            players = [Player('Eric'), Player('Bob'), Player('Phil')]
        self.cards = cards
        self.nobles = nobles
        self.players = players
        self.number_of_tokens_per_type = len(self.players) + 2

        self.discovered_cards = self.distribute_cards()
        self.discovered_nobles = self.distribute_nobles()

    def take_tokens(self, tokens):
        if tokens.total() > 3:
            raise InvalidAmountException('You can not take more than three tokens')

        if self.get_current_player().tokens.total() + tokens.total() > 10:
            raise InvalidAmountException('You can have at most 10 tokens at the same time')

        if tokens.total() == 3 and len(tokens.get_available_types()) == 2:
            raise InvalidAmountException('You can not take 2 tokens of the same color plus another token')

        if self.tokens.total() < tokens.total():
            raise InvalidAmountException(f"There is only {self.tokens.total()} tokens left, asked for {tokens.total()}")

        if tokens not in self.tokens:
            raise InvalidAmountException('Invalid Tokens Request (asked {} in {})'.format(tokens, self.tokens))

        if tokens.has('m'):
            raise MagicTokenRequestException('You can not take magic tokens')

        if self.tokens.total() == 2 and len(tokens.get_available_types()) == 1:
            token_type = tokens.get_available_types()[0]

            if not self.tokens.has(token_type, min=4):
                raise InvalidAmountException(
                    'You can only take 2 tokens when there is at least 4 tokens of the same type')

        self.tokens.remove_all(tokens)
        self.get_current_player().tokens.add_all(tokens)

        return self

    def reserve_card_from_the_stack(self, age):
        player = self.get_current_player()
        card = None

        if len(player.reserved_cards) == self.reserved_cards_limit:
            raise ReservedCardsInventoryFullException(
                f"You reached the limit of {self.reserved_cards_limit} reserved cards")

        for card_in_stack in self.cards:
            if card_in_stack.age == age:
                card = card_in_stack
                break

        if card is None:
            raise EndOfStackException('Trying to reserve a card from an empty stack of cards (wtf)')

        player.reserved_cards.append(card)
        self.cards.remove(card)

        return self

    def reserve_card(self, card: Card):
        player = self.get_current_player()

        if len(player.reserved_cards) == self.reserved_cards_limit:
            raise ReservedCardsInventoryFullException(
                f"You reached the limit of {self.reserved_cards_limit} reserved cards")

        if self.tokens.has('m', 1):
            player.tokens.add('m', 1)
            self.tokens.remove('m', 1)

        player.reserved_cards.append(card)
        self.discovered_cards[card.age].remove(card)
        self.discovered_cards = self.distribute_cards(self.discovered_cards)

        return self

    def build_card(self, card: Card):
        player = self.get_current_player()

        if card.cost not in player.get_all_tokens():
            raise CardTooExpensiveException('You can not afford this card.')

        player.tokens.remove_all(card.cost - player.get_cards_production(), True)
        self.tokens.add_all(card.cost - player.get_cards_production())
        player.cards.append(card)
        player.points += card.points

        if card in player.reserved_cards:
            player.reserved_cards.remove(card)
        else:
            self.discovered_cards[card.age].remove(card)
            self.discovered_cards = self.distribute_cards(self.discovered_cards)

        return self

    def distribute_cards(self, cards=None):
        if cards is None:
            cards = [[], [], []]

        i = 0

        while not all(4 == len(age) for age in cards):
            card = random.choice(self.cards)

            if i > 120:
                raise EndOfStackException('No cards left')

            if len(cards[card.age]) < 4:
                self.cards.remove(card)
                cards[card.age].append(card)
            else:
                i += 1
        return cards

    def get_available_nobles(self):
        player = self.get_current_player()
        available = []

        for noble in self.discovered_nobles:
            if noble.requirements in player.get_cards_production():
                available.append(noble)

        return available

    def consume_noble(self, noble):
        player = self.get_current_player()

        if noble not in self.get_available_nobles():
            raise NotFamousEnoughException('Trying to consume a noble not interested by *you*')

        player.nobles.append(noble)
        self.discovered_nobles.remove(noble)

        return noble

    def has_won(self, player):
        return player.points >= self.points_limit

    def distribute_nobles(self):
        nobles = []

        while len(nobles) < len(self.players) + 1:
            nobles.append(
                random.choice(self.nobles)
            )

        return nobles

    def get_current_player(self):
        return self.players[self.playing]

    def next_player(self):
        self.get_current_player().turns_played += 1
        self.turns += 1
        self.playing += 1

        if self.playing == len(self.players):
            self.playing = 0

        return self

    def ended(self):
        for player in self.players:
            if self.has_won(player):
                return True
        return False
